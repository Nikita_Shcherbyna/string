package task6;

import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class StartGame {
    Scanner in = new Scanner(System.in);
    private boolean isWordGuessed = false;

    public void startPage() {
        System.out.println("-----Welcome to the Guess word game!------");
        System.out.println("Word list that you need to use during the game : \n" +
                "apple, orange, lemon, banana, apricot \n" +
                "avocado, broccoli, carrot, cherry, garlic \n" +
                "grape, melon, leak, kiwi, mango \n" +
                "mushroom, nut, olive, pea, peanut, pear \n" +
                "pepper, pineapple, pumpkin, potato \n" +
                "------------------------------------------");

        System.out.println("Now guess the word!");
    }

    public void start() {
        String[] words = {"apple", "orange", "lemon", "banana", "apricot", "avocado", "broccoli", "carrot",
                "cherry", "garlic", "grape", "melon", "leak", "kiwi", "mango", "mushroom", "nut", "olive",
                "pea", "peanut", "pear", "pepper", "pineapple", "pumpkin", "potato"};

        while (!isWordGuessed){
            Random randomWordFromArray = new Random();
            String wordFromArray = words[randomWordFromArray.nextInt(words.length)];
            System.out.println(wordFromArray.substring(0,2).concat("#############"));
            String playerWord = in.nextLine().toLowerCase(Locale.ROOT).trim();
            if (!wordFromArray.equals(playerWord)){
                System.out.println("You lost the game!");
                System.out.println("The correct word is " + wordFromArray + " try again");
            }else{
                System.out.println("You won the game");
                isWordGuessed = true;
            }
        }
    }
}