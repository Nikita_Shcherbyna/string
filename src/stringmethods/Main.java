package stringmethods;

import java.util.Locale;

public class Main {
    public static void main(String[] args) {
        int method1 = Methods.findSymbolOccurance("apple", 'p');
        System.out.println(method1);

        int method2 = Methods.findWordPosition("Apple".toLowerCase(Locale.ROOT), "le".toLowerCase(Locale.ROOT));
        System.out.println(method2);

        StringBuilder method3 = Methods.stringReverse("Hello World!");
        System.out.println(method3);

        boolean method4 = Methods.isPalindrome(" Kek    ".toLowerCase(Locale.ROOT).trim());
        System.out.println(method4);
    }
}
