package stringmethods;

public class Methods {
    public static int findSymbolOccurance(String word, char symbol){
        int letterCounter = 0;
        char[] letters = word.toCharArray();
        for (int i = 0; i < letters.length; i++){
            if (letters[i] == symbol){
                letterCounter++;
            }
        }
        return letterCounter;
    }

    public static int findWordPosition(String source, String target){
        int i = source.indexOf(target);
        if (source.length() >= target.length() && source.contains(target)){
            return source.indexOf(target);
        }else {
            return -1;
        }
    }

    public static StringBuilder stringReverse(String word){
        StringBuilder str = new StringBuilder(word);
        str.reverse().toString();
        return str;
    }
    //another way of solution by using array

    public static boolean isPalindrome(String word){
        StringBuilder str = new StringBuilder(word);
        if (str.reverse().toString().equals(word)){
            return true;
        }else {
            return false;
        }
    }
}
